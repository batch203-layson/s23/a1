// console.log("Hello Worlds");


let trainer = 
{
    Name: "Ash Ketchum",
    Age: 10,
    Pokemon:["Pikachu","Charizard","Squirtle","Bulbasur"],
    friends: 
    {
        hoenn: ["May","Max"],
        kanto: ["Brock", "Misty"]
    },
    talk:   function()
    {
        console.log("Result of talk method");
        console.log(this.Pokemon[0]+" I choose you!");
    }
};
console.log(trainer);
// console.log(typeof trainer);

function resultOne()
{
    console.log("Result of dot notation");
    console.log(trainer.Name);
}

resultOne();


function resultTwo()
{
    console.log("Result of square bracket notation");
    console.log(trainer.Pokemon);
}

resultTwo();


function resultThree()
{
    console.log(trainer.talk());
}

resultThree();
/* 
3. Create a trainer object using object literals.
4. Initialize/add the following trainer object properties:
a. Name (String)
b. Age (Number)
c. Pokemon (Array)
d. Friends (Object with Array values for properties)
5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
*/



// console.log("Result of dot notation:");
/* 
6. Access the trainer object properties using dot and square bracket notation.
7. Invoke/call the trainer talk object method.
*/
